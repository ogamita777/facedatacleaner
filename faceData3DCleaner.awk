BEGIN {
    x[0]=0
    x[1]=1
    x[2]=7
    x[3]=9
    x[4]=10
    x[5]=15
    x[6]=16
    x[7]=17
    x[8]=18
    x[9]=19
    x[10]=20
    x[11]=21
    x[12]=22
    x[13]=23
    x[14]=24
    x[15]=30
    x[16]=31
    x[17]=32
    x[18]=33
    x[19]=34
    x[20]=40
    x[21]=41
    x[22]=48
    x[23]=49
    x[24]=50
    x[25]=51
    x[26]=53
    x[27]=56
    x[28]=63
    x[29]=64
    x[30]=65
    x[31]=66
    x[32]=79
    x[33]=80
    x[34]=81
    x[35]=82
    x[36]=83
    x[37]=84
    x[38]=85
    x[39]=86
    x[40]=87
    x[41]=88
    x[42]=89
    x[43]=90
    x[44]=91
}
{
    selectedSize = 0
    columnSize = split($0, currentValue, ",")
    
    for(i = 0; i < columnSize; i++){
        k = 0
        m = int(i / 3)
        # 現在の列が指定されている列のものかどうか
        for(l = 0; l < 45; l++){
            if(x[l] == m) {
                selectedSize++
                k = 1
            }
        }
        
        if(k == 1) {
            if(NR == 1) {
                printf("%s,",currentValue[i+1])
            } 
            if(NR == 2) { 
                printf("%.6f,",currentValue[i+1])  
                savedSelectedValue[selectedSize] = currentValue[i+1]
            } else if(NR > 2) { 
                tmpValue[selectedSize] = currentValue[i+1]  
            }
        }
    }
    if(NR == 1 || NR == 2) {
        printf("\n")
    }
    
    if(NR > 2) {
        z = 0
        # 保存している行と現在選択している行が等しいかどうかを判定
        for(q = 0; q < selectedSize; q++) {
            if(abs(tmpValue[q+1] -  savedSelectedValue[q+1]) >= 0.000001) {
                z = 1
            }
        }
        # 新しい行が見つかった時、savedSelectedValueを新しい行の値に更新して値を出力
        if(z == 1) {
            for(f=0; f < selectedSize; f++) {
                printf("%.6f,",tmpValue[f+1])  
                savedSelectedValue[f+1] = tmpValue[f+1] 
            }
            printf("\n")
        }
    }
}

function abs(x) {
    return (x>0) ? x : -x
}

