{
    selectedSize = 0
    columnSize = split($0, currentValue, ",")
    
    for(i = 0; i < columnSize; i++){
        if(NR == 1) {
            printf("%s,",currentValue[i+1])
        } else if(NR == 2) { 
            printf("%.6f,",currentValue[i+1])  
            savedSelectedValue[i+1] = currentValue[i+1]
        } else if(NR > 2) { 
            tmpValue[i+1] = currentValue[i+1]  
        }
 
    }
    if(NR == 1 || NR == 2) {
        printf("\n");
    }
    
    if(NR > 2) {
        z = 0
        # 保存している行と現在選択している行が等しいかどうかを判定
        for(q = 0; q < columnSize; q++) {
            if(abs(tmpValue[q+1] - savedSelectedValue[q+1]) >= 0.000001) {
                z = 1
            }
        }
        # 新しい行が見つかった時、savedSelectedValueを新しい行の値に更新して値を出力
        if(z == 1) {
            for(f = 0; f < columnSize; f++) {
                printf("%.6f,",tmpValue[f+1])  
                savedSelectedValue[f+1] = tmpValue[f+1] 
            }
            print ""
        }
    }
}

function abs(x) {
    return (x>0) ? x : -x
}
