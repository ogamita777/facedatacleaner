nkf -Lu --overwrite $1
echo $1
if [[ $1 =~ .*3D.* ]]
then
    awk -f faceData3DCleaner.awk $1 > ${1%/*}/cleanly_${1##*/}
else
    awk -f faceDataETCCleaner.awk $1 > ${1%/*}/cleanly_${1##*/}
fi