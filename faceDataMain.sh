for folderName in *
do
    if [ -d $folderName ]
    then
        for folderName2 in $folderName/* # folderName2にはルートからそこまでのパスが入る
        do
            if [ -d $folderName2 ]
            then
                for fm in $folderName2/*
                do
                    if [ -f $fm ] # fmにはルートからそこまでのパスが入る
                    then
                        ./faceDataCleaner.sh $fm
                    fi
                done
            fi
        done
    fi
done
